(function(){
	angular
		.module("cordovaApp")
		.controller("HomeCtrl", HomeCtrl);

		HomeCtrl.$inject = ["$cordovaDialogs", "$cordovaSocialSharing", "$ionicPlatform", "$http", "$state", "$scope", "LoginModal"];

		function HomeCtrl($cordovaDialogs, $cordovaSocialSharing, $ionicPlatform, $http, $state, $scope, LoginModal){
			var vm = this;

			vm.nativeAlert = function(){
				$cordovaDialogs
					.alert('Message', 'Title Goes Here', 'OK')
					.then(function(){
						//do something
					});
			};

			vm.login = function(){
				LoginModal
					.initialize($scope)
					.then(function(mdl){
						mdl.show();
					});
			};

			vm.shareThis = function(){
				$cordovaSocialSharing
					//.share(message, subject, file, link)
					.share(null, null, null, "http://google.com")
					.then(function(result){
						//do something
					}, function(err){
						//do something
					});
			}

			$ionicPlatform.ready(function(){
				console.log("Entered!");
			});
		}
})();