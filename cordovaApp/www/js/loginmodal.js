(function(){
	angular
		.module("cordovaApp")
		.service("LoginModal", LoginModal);

		LoginModal.$inject = ["$ionicModal", "$rootScope", "$cordovaFacebook"];
		
		function LoginModal($ionicModal, $rootScope, $cordovaFacebook){
			var vm = this;

			vm.initialize = function($scope){
				var parent = $scope;
				var self = $scope || $rootScope.new();

				var promise = $ionicModal.
				fromTemplateUrl('templates/loginmodal.html', {
			      scope: self,
			      animation: 'slide-in-up'
			    }).then(function(modal){
			      self.modal = modal;
			      return modal;
			    });

			    self.closeMe = function(){
			    	self.modal.hide();
			    }

			    self.loginFacebook = function(){
			    	console.log("Facebook Login!");

			    	$cordovaFacebook
			    		.login(["public_profile", "email", "user_friends"])
			    		.then(function(success){

			    			$cordovaFacebook
			    				.api("me", ["public_profile"])
			    				.then(function(success){
			    					//sample response: {"name":"Fsf Singapore","id":"211293779315682"}
			    					alert(success.name);
			    				}, function(err){
			    					//do something if err
			    				});

			    		}, function(error){
			    			//do something if err
			    		});

			    }

			    return promise;
			}
		}
})();