(function(){

	angular
		.module("starter")
		.service("LoginModal", LoginModal);

	LoginModal.$inject = ["$ionicModal", "$rootScope", "$ionicSlideBoxDelegate"];

	function LoginModal($ionicModal, $rootScope, $ionicSlideBoxDelegate){

		var vm  = this;

		vm.initialized = function($scope){
			var parent = $scope; //from parent
			var self = $scope || $rootScope.new();

			var promise = $ionicModal.
				fromTemplateUrl('templates/login-modal.html', {
					scope: self,
					animation: 'slide-in-up'
				}).then(function(modal){
					self.modal = modal;
					return modal;
				});

			self.closeMe = function(){
				self.modal.hide();
			}

			self.nextSlide = function(){
				$ionicSlideBoxDelegate.next();
				//$ionicSlideBoxDelegate.prev();
			}

			return promise;
		}
	}

})();