(function(){

	angular
		.module("starter")
		.controller("LoginCtrl", LoginCtrl)
		.controller("HomeCtrl", HomeCtrl);

	LoginCtrl.$inject = ["$state", "$http"];

	function LoginCtrl($state, $http){
		var vm = this;

		vm.user = {
			email: '',
			password: ''
		}

		vm.errorMsg = '';

		vm.doLogin = function(){
			if(vm.user.email == ""){
				vm.errorMsg = "Email can't be empty";				
			}else if(vm.user.password == ""){
				vm.errorMsg = "Password can't be empty";				
			}else{

				$http({	
					method: "POST",
					url: "http://demo5964607.mockable.io/login",
					data: { "email": vm.user.email, "password": vm.user.password },
					headers: {
						'Content-type': 'application/json'
					}
				}).then(function successCallBack(response){
					//check response
					$state.go("home");
				}, function errorCallBack(){
					//do something i.e logging
				});

			}
		}
	}	

	HomeCtrl.$inject = ["$state"];

	function HomeCtrl($state){
		var vm = this;
		vm.doLogout = function(){
			$state.go("login");
		}
	}

})();